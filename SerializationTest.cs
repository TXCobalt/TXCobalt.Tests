﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TXCobalt.Core;

namespace TXCobalt.Tests
{
    [TestClass]
    public class SerializationTest
    {
        [TestMethod]
        public void TestSerializeGameObjects()
        {
            Console.WriteLine("Try serialize unique data");
            File.WriteAllBytes("test.pack2", GameObjectContainer.Serialize(new BulletObject() { transform = new Transform(1, 5, Tilt.Left), GUID = Guid.NewGuid(), bulletdata = new BulletObjectData(Color.White, Guid.NewGuid())}));
            Console.WriteLine("Try deserialize unique data");
            GameObjectContainer container = (GameObjectContainer)GameObjectContainer.Deserialize(File.ReadAllBytes("test.pack2"), false);

            Console.WriteLine(container.Guid);
            Console.WriteLine(container.transform);
            Console.WriteLine(container.TypeId);
            Console.WriteLine(container.ObjectData.GetType());
        }

        [TestMethod]
        public void SerializeGameRules()
        {
            Console.WriteLine("Test export");
            GameRules rules = GameRules.Default;
            GameRules.ExportGameRules("rules.json", rules);
            Console.WriteLine("Test import rules");
            GameRules newrules = GameRules.ImportGameRules("rules.json");
            Console.WriteLine("{0}: {1}", "AdvancedConsoleDebugging", newrules.AdvancedConsoleDebugging);
            Console.WriteLine("{0}: {1}", "AlternativeBotAlgoritm", newrules.AlternativeBotAlgoritm);
            Console.WriteLine("{0}: {1}", "AdvancedConsoleDebugging", newrules.BotAmount);
            Console.WriteLine("{0}: {1}", "BotAmount", newrules.EnableAntiHack);
            Console.WriteLine("{0}: {1}", "GameTick", newrules.GameTick);
            Console.WriteLine("{0}: {1}", "MaxGameObject", newrules.MaxGameObject);
            Console.WriteLine("{0}: {1}", "MaxPlayer", newrules.MaxPlayer);
            Console.WriteLine("{0}: {1}", "NoClip", newrules.NoClip);
            Console.WriteLine("{0}: {1}", "OptimisedBotAlgoritm", newrules.OptimisedBotAlgoritm);
        }
        [TestMethod]
        public void SerializeRespose()
        {
            ServerResponse response = new ServerResponse() { IsAvailable = true, Map = "Default", MaxPlayer = 64, Moded = false, MOTD = "Bienvenu sur le server officiel de TXCobalt", PasswordProtected = false, PlayerCount = 24, ProtocolVersion = "dev-0.1", UseAlternateSerialization = false };
            File.WriteAllBytes("Response.json", (response.Serialize()));
        }
        public void SerializeBullet()
        {
        }
    }
}
